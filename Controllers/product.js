const products = [
    { title: 'Mobile Phone', id: 1 },
    { title: 'Fridge', id: 2 },
    { title: 'Bags', id: 3 }
]

exports.getAllProduct = (req, res, next) => {
    console.log(products);
    res.json(products)
}

exports.addProduct = (req, res, next) => {

    if (!req.body) {
        res.status(404).json("Please Enter the title of the product");
        return;
    }

    let productIds = products.map(item => item.id);
    // create new id (basically +1 of last item object)
    let newId = productIds.length > 0 ? Math.max.apply(Math, productIds) + 1 : 1;
    let newProduct = {
        title: req.body.title,
        id: newId
    }
    products.push(newProduct);
    res.json(newProduct);
}

exports.getProductById = (req, res, next) => {
    const product = products.find(c => c.id === parseInt(req.params.id));

    if (!product) {
        res.status(404).json("Product Does Not exist");
        return;
    }
    res.send(product);
}