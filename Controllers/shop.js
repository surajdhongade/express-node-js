
const shops = [
    { shopName: "Khandalwala kirana and rashan store", emailId: "khandalwala@gmail.com", contactNo: 9939193913, id: 1 },
    { shopName: "lalnas chaha", emailId: "lalnas@gmail.com", contactNo: 8732142843, id: 2 },
    { shopName: "haldiram mithaiwala", emailId: "haldiram@gmail.com", contactNo: 7843273726, id: 3 }
]

exports.getAllShop = (req, res, next) => {
    if (!shops) {
        res.status(404).json("No shops entered till now");
        return;
    }

    res.status(200).send(shops);
}

exports.getShopById = (req, res, next) => {
    const shop = shops.find(c => c.id == req.params.id);

    if (!shop) {
        res.status(404).json("shop does not exist");
        return;
    }

    res.status(200).send(shop)
}