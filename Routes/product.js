const express = require('express');

const router = express.Router();

const productController = require('../Controllers/product')

router.post('/add-product', productController.addProduct)

router.get('/getAllProduct',productController.getAllProduct)

router.get('/getProductById/:id',productController.getProductById)

module.exports = router;