const express = require('express');

const router = express.Router();

const shopController = require('../Controllers/shop')

router.get('/getAllShop',shopController.getAllShop)

router.get('/getshopById/:id', shopController.getShopById)

module.exports = router;