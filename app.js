const http = require('http');

// third party packages
const express = require('express');
const bodyParser = require('body-parser')

const productRoutes = require('./Routes/product');
const shopRoutes = require('./Routes/shop')

const app = express();

app.use(bodyParser.urlencoded({ extended: true })); // to encode the incoming url request
app.use(bodyParser.json());//to catch incoming json payload

// app.use('/', (req, res, next) => {
//     console.log("This Middleware always call");
//     next(); // to call another use() method to execute the middleware
// })

// use() allow us to use new middle ware function .. passing function to the use() method with three parameter

app.use('/api/product', productRoutes);
app.use('/api/shop', shopRoutes);

app.use('/', (req, res, next) => {
    console.log("In Another middleware");
    res.send("404 Not Found")
    // res.json("Endpoint does not exist");
})

// const server = http.createServer(app);
// server.listen(3000);
// both inside 
app.listen(3000)