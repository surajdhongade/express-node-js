const swaggerJSDoc = require("swagger-jsdoc");
const express = require("express");
const app = express();


const swaggerDefinition = {
    info: {
        swagger: "2.0",
        title: 'Product API',
        description: 'Gold Loan APIs',
    }
};
const options = {
    swaggerDefinition,
    apis: ['./Routes/*.js'],
};

module.exports.swaggerSpec = swaggerJSDoc(options);
app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});